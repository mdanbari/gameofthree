package game;

import java.io.IOException;
import java.net.UnknownHostException;

public class Main {
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		ServerPlayer serverPlayer = new ServerPlayer(8585);
		ClientPlayer clientPlayer = new ClientPlayer("localhost", 8585);
		
		(new Thread(serverPlayer)).start();
		(new Thread(clientPlayer)).start();
		
		
	}

}
