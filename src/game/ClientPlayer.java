package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

public class ClientPlayer implements Runnable {

	Socket clientSocket;
	PrintWriter out;
	BufferedReader in;
	String server;
	int port;
	boolean generatedRandom = false;
	static final int MIN_BOUND_RAND = 1000000;
	static final int MAX_BOUND_RAND = 1000000000;

	public ClientPlayer(String server, int port) throws UnknownHostException, IOException {
		this.server = server;
		this.port = port;
	}

	@Override
	public void run() {
		try {
			clientSocket = new Socket(this.server, this.port);
			System.out.println("Player Client Starts Game!");
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			out = new PrintWriter(clientSocket.getOutputStream(), true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		while (true) {
			try {
				if (!generatedRandom) {
					int randomNum = generateRand(MAX_BOUND_RAND, MIN_BOUND_RAND);
					out.println(randomNum);
					out.flush();
					System.out.println("[ClientPlayer] Generate random number " + randomNum + " !! ");
					generatedRandom = true;
				}
				String inputLine = in.readLine();
				System.out.println("[ClientPlayer] Recieving number from ServerPlayer: " + inputLine);
				int clientNumber = Integer.valueOf(inputLine);
				int mod = clientNumber % 3;
				int numToAdd = mod != 0 ? 3 - mod : 0;
				int dividedByThree = (clientNumber + numToAdd) / 3;
				if (dividedByThree == 1)
					break;
				else {
					System.out.println("[ClientPlayer] send  " + dividedByThree + " to ServerPlayer");
					out.println(dividedByThree);
					out.flush();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("ClientPlayer is WIN !!!!");

	}

	public int generateRand(int max, int min) {
		Random rand = new Random();
		int nextInt = rand.nextInt((max - min) + 1) + min;
		return nextInt;

	}

}
