package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerPlayer implements Runnable {

	ServerSocket serverSocket;
	PrintWriter out;
	BufferedReader in;
	int port;

	public ServerPlayer(int port) throws IOException {
		this.port = port;
		serverSocket = new ServerSocket(this.port);

	}

	@Override
	public void run() {
		try {
			System.out.println("Player Server Starts Game!");
			Socket client = serverSocket.accept();
			in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			out = new PrintWriter(client.getOutputStream(), true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		while (true) {
			try {

				String dataFromClient = in.readLine();
				System.out.println("[ServerPlayer] Recieving number from ClientPlayer: " + dataFromClient);
				int clientNumber = Integer.parseInt(dataFromClient);
				int mod = clientNumber % 3;
				int numToAdd = mod != 0 ? 3 - mod : 0;
				int dividedByThree = (clientNumber + numToAdd) / 3;
				if (dividedByThree == 1)
					break;
				else {
					System.out.println("[ServerPlayer] send  " + dataFromClient + " to CleintPlayer");
					out.println(dividedByThree);
					out.flush();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.out.println("ServerPlayer is WIN !!!!");

	}

}
