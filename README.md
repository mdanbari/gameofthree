## The problem: Game of Three - Coding Challenge


The goal is to implement a game with two independent agents � the �players� � 
communicating with each other using an interface. 

When a player starts, a random whole number is generated and sent to the other player, which indicates the start of the game. The receiving player must now add one of { -1, 0, 1} to get a number that is divisible by 3 and then divide it. The resulting number is then sent back to the original sender. 

The same rules apply until one of the players reaches the number 1 after division, which ends the game.

## My Solution and Design:

I used TCP interface for communicating between two players. 
TCP is a two-way communication protocol; hence data can be sent across both streams at the same time.

in this scenario, one player acts as a server and creates a server socket, bound it to a port number and wait for a client. After the server is waiting, a client player instantiates a Socket object, specifying the server name and the port number to connect to.

After the connection is established, communication between two players can occur using I/O streams.
Each socket has both an OutputStream and an InputStream. The client player's OutputStream is connected to the server player's InputStream, and vice versa.

In this scenario when the client player connects to server player socket, it generates a random integer number between 1000000 to 1000000000 and sends it to the server player and the game begin.

## Independent Process:

For running each process in an independent process I implemented each client Player and Server Player Class from Runnable interface, in order that each player has its own process and run independently from Main Class as below:

```ruby
(new Thread(serverPlayer)).start();
(new Thread(clientPlayer)).start();
```


## How to run Application:

After Exporting project as a jar file If it's not an executable JAR, then you'll need to run the program with something like:

```
java -cp game-of-three.jar game.Main
```

## Sample Result:
```
Player Server Starts Game!
Player Client Starts Game!
[ServerPlayer] Recieving number from ClientPlayer: 286550794
[ServerPlayer] send  286550794 to CleintPlayer
[ClientPlayer] Generate random number 286550794 !! 
[ClientPlayer] Recieving number from ServerPlayer: 95516932
[ClientPlayer] send  31838978 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 31838978
[ServerPlayer] send  31838978 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 10612993
[ClientPlayer] send  3537665 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 3537665
[ServerPlayer] send  3537665 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 1179222
[ClientPlayer] send  393074 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 393074
[ServerPlayer] send  393074 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 131025
[ClientPlayer] send  43675 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 43675
[ServerPlayer] send  43675 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 14559
[ClientPlayer] send  4853 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 4853
[ServerPlayer] send  4853 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 1618
[ClientPlayer] send  540 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 540
[ServerPlayer] send  540 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 180
[ClientPlayer] send  60 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 60
[ServerPlayer] send  60 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 20
[ClientPlayer] send  7 to ServerPlayer
[ServerPlayer] Recieving number from ClientPlayer: 7
[ServerPlayer] send  7 to CleintPlayer
[ClientPlayer] Recieving number from ServerPlayer: 3
ClientPlayer is WIN !!!!
```


